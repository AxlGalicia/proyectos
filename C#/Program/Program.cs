﻿using System;

namespace Desktop
{
  class Program
  {

	const char BLANK =' ';
	const char DOT ='.';
	const char X = 'x';
	const int cMaxLineChars=79;
	const int cHalf= cMaxLineChars/2;
	static char[] LINE = new char[cMaxLineChars];
	static char[] imprimir = new char[50];
	delegate double FUNC(double X);


	static void Main(string[] args)
	{
		
       // Console.WriteLine("Aqui voy!!!!!!s");
		DoThePlot(Math.Sin);
	

		Console.ReadKey();
		Console.Clear();
	}

	static void DoThePlot(FUNC TheDelegate)
	{
       // Console.WriteLine("Aqui voy!!!!!!s");
	    //Console.WriteLine(LINE);
		fillUp(LINE,WithChar: DOT);
		Console.WriteLine(LINE);
		fillUp(LINE,WithChar: BLANK);
		PlotFunc(TheDelegate);
	}

	static void fillUp(char[] line, char WithChar= '\0')
	{
        //Console.WriteLine(line);
        //Console.WriteLine(WithChar);
        //Console.WriteLine(line.Length);
		for(int i=0;i<line.Length;i++)
		{
			line[i]=WithChar;
		}
		
	}

	static void PlotFunc(FUNC f)
	{
		char[,] numero;
        numero = new char[79,50];
		int contador=0;
        for(int fila=0;fila<79;fila++)
        {
            for(int col=0;col<50;col++)
            {
                   numero[fila,col]=BLANK;
            }

        }



		double maxval =10.0;
		double delta =0.2;
		int loc;
		LINE[cHalf]=DOT;
		for(double x=0.0001;x<maxval;x+=delta)
		{
			loc=(int)Math.Round(f(x)*cHalf)+cHalf;
		//	Console.WriteLine(f(x));
		//	Console.WriteLine(loc);
			LINE[loc]= X;
		
			numero[loc,contador]=X;
			contador++;

			
			Console.WriteLine(LINE);
			fillUp(LINE,WithChar:BLANK);
			LINE[cHalf]=DOT;
		}
		//char [] imprimir;
		//imprimir = new char[15];
	    for(int cs=0;cs<50;cs++){
			imprimir[cs]=' ';
		}

		for(int fila=0;fila<79;fila++)
        {
			for(int cs=0;cs<50;cs++)
			{
			imprimir[cs]=' ';
		    }
            for(int col=0;col<50;col++)
            {
                 imprimir[col]=numero[fila,col];
				 if(col==49){
					 Console.WriteLine(imprimir);
				 }
            }

        }


	}

  }
}
