﻿using System;

namespace GraficaSeno
{
    class Program
    {


        const char VACIO =' ';
	    const char ASTERISCO = '*';
    	const int VectorMax=79;
    	const int MitadVector= VectorMax/2;
	    static char[] VectorLinea = new char[VectorMax];

        delegate double FUNC(double X);

        static char[] imprimir = new char[50];

        static char[,] Matriz= new char[VectorMax,50];
       
        
        static void Main(string[] args)
        {

            GraficarFuncion(Math.Sin);
           
            Console.ReadKey();
		    Console.Clear();
        }



        static void GraficarFuncion(FUNC f)
	    {
            int contador=0;
            LimpiarMatriz(Matriz,Caracter: VACIO);

            double maxval =12.0;
		    double delta =0.2;
		    int Posicion;
		    for(double x=3.00;x<maxval;x+=delta){
                Posicion=(int)Math.Round(f(x)*MitadVector)+MitadVector;
                VectorLinea[Posicion]= ASTERISCO;
                Matriz[Posicion,contador]=ASTERISCO;
			    contador++;
			    LLenadoVector(VectorLinea,Caracter:VACIO);

            }

            Print();



        }

        static void LLenadoVector(char[] linea, char Caracter= '\0')
	    {
		for(int i=0;i<linea.Length;i++)
		{
			linea[i]=Caracter;
		}
	    }
        static void LimpiarMatriz(char[,] MatrizB, char Caracter){

            for(int fila=0;fila<79;fila++)
            {
                for(int col=0;col<50;col++)
                {
                    MatrizB[fila,col]=Caracter;
                }

            }

        }
        static void Print(){
            for(int cs=0;cs<50;cs++)
            {
			    imprimir[cs]=' ';
		    }

		    for(int fila=0;fila<79;fila++)
            {
			    for(int cs=0;cs<50;cs++)
			    {
			        imprimir[cs]=' ';
		        }
                for(int col=0;col<50;col++)
                {
                     imprimir[col]=Matriz[fila,col];
				     if(col==49)
                     {
					 Console.WriteLine(imprimir);
				     }   
                }   
            }
        }
    }
}
