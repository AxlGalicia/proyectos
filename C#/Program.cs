using System;

namespace Desktop
{
  class Program
  {

	const char BLANK ='';
	const char DOT ='* ';
	const char X = 'x';
	const int cMaxLineChars=79;
	const int cHalf= cMaxLineChars/2;
	static char[] LINE = new char[cMaxLineChars];
	delegate double FUNC(double X);

	static void Main(string[] args)
	{
		DoThePlot(Math,Sin);
		Console.ReadKey();
		Console.Clear();
	}

	static void DoThePlot(FUNC TheDelegate)
	{
		fillUp(LINE,WithChar: DOT);
		Console.WriteLine(LINE);
		fillUp(LINE,WithChar: BLANK);
		PlotFunc(TheDelegate);
	}

	static void fillUp(char[] line, char WithChar= '\0')
	{
		for(int i=0;1<line.Length;i++)
		{
			line[i]=WithChar;
		}
	}

	static void PlotFunc(FUNC f)
	{
		double maxval =9.0;
		double delta =0.2;
		int loc;
		LINE[cHalf]=DOT;
		for(double x=0.0001;x<maxval;x+=delta)
		{
			loc=(int)Math.Round(f(x)*cHalf)+cHalf;
			LINE[loc]= X;
			Console.WriteLine(LINE);
			fillUp(LINE,WithChar:BLANK);
			LINE[cHalf]=DOT;
		}
	}

  }
}
