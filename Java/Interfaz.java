
import java.awt.*;

public class Interfaz extends javax.swing.JFrame {
    int x0,xN,y0,yN;  //X0 y Y0 posiciones iniciales XN y YN posiciones a cambiar
    double xmin,xmax,ymin,ymax;  //valores limite de la funcion
    int apAncho,apAlto;  //Valores para calcular la altura y ancho de la funcion

    public Interfaz() {
        initComponents(); //Se inicializa la interfaz
        initVariables();  //Se inicializa las variables
    }

    public void initVariables() {
        Dimension d = getSize();
        apAncho = d.width;
        apAlto = d.height;

        
        x0 = y0 = 0;
        xN = apAncho-1;
        yN = apAlto-1;
        xmin = -10.0;
        xmax = 10.0;
        ymin = -1.0;
        ymax = 1.0;
    }

    public void paint( Graphics g ) {
       
        int j1,j2;

        j1 = ValorY( 0 );
        for( int i=0; i < apAncho; i++ )
            {
            j2 = ValorY( i+1 );
            g.drawLine( i,j1,i+1,j2 );
            g.setColor(Color.RED);
            j1 = j2;
            }
    }

    private int ValorY( int valor ) {
        double x,y;
        int retorno;

        x = (valor * (xmax-xmin) / (apAncho-1)) + xmin;

        y = Math.sin( x );

        retorno = (int)( (y-ymin) * (apAlto-1) / (ymax-ymin) );

        retorno = apAlto - retorno;

        return( retorno );
    }




    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 600, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Interfaz().setVisible(true);
            }
        });
    }
}
