import numpy as n
import matplotlib.pyplot as mplot

x=n.arange(0,4*n.pi,0.1)
y=n.sin(x)
mplot.plot(x,y)
mplot.show()

